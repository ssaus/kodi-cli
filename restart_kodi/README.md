
This is a series of scripts to allow starting and restarting Kodi/XMBC from a 
cron job, ssh connection, monit, and so on.  Yes, you could use systemd to do 
something similar, but you'll still have the display problems.

As a side note, this can also make it easier for you to forward X across ssh 
for another user.  Yes, there are *definitely* security risks in doing this, so 
be careful.

## Why

* A terminal connection doesn't have access to the X display without messing with `xauth`.  
* Kodi does not always listen properly to JSON commands for reasons that completely mystify me.
* Somewhere in my setup - probably in an add-on or when it scans for new media - there is a memory leak. Kodi goes from ~500M memory usage to ~5G memory usage over a day or three, depending on usage.  A quick restart fixes that entirely.

## Moving Parts

* `get_xauth.sh` : To be run on initial login. It gets the first line of `xauth list` and tosses it into a file. **change the filename where it stores this info**
* `add_xauth.sh` : To be run by the scripts to ensure the process has access to `DISPLAY :0`. **change the filename it pulls this info from**
* `kodi-safe-startup.sh` : This gets the xauth string and adds it for the user running the script, then uses a cascade of if/then statements to make sure network drives are up before launching Kodi on `DISPLAY :0`.  If you like, instead of just having it wait 10 seconds between each check, you can add in a command to re-attempt the mounting of your external drives. Specifically designed to *NOT* rely on systemd.
* `kodi-quit.sh` : A very, VERY stripped down version of the `kodi-cli` script that looks for a `.kodirc` in the current directory (alternately, specified as the only argument on the commandline, e.g. `kodi-quit.sh /path/to/.kodirc`) and issues a JSON request via curl to shut down kodi. 
* `kodi-restart.sh` : This is an example that both shows how to use these scripts in practice. Kodi will launch a second instance, even if the first is still running, so there are checks to first use `kill` and then `kill -9` to kill the process if it's being obstinate.  You will need to edit your `sudoers` file for that part to work, see below.
* `xauth.desktop` : Intended to go in your autostart directory, preferably `$HOME/.config/autostart`. **You will have to edit the `Exec=` line with the correct path to `get_xauth`**.  Alternately, you can add the `Exec` command to whatever autostart mechanism you use, e.g. Openbox's `$HOME/.config/openbox/autostart`
* `.kodirc` : This is the configuration file; see below.
* `psx` : This is a little utility I wrote (MIT license, NOT GNU license!) that is part of a collection of process control scripts that will be in its own repository later.  

## Installation

* Put all these files in the same directory, your choice.
* Edit `get_xauth.sh`, `add_xauth.sh`, and `xauth.desktop` appropriately.
* Copy `xauth.desktop` to `$HOME/.config/autostart`.
* Run `get_xauth.sh` at least once (or reboot if you've added it to autostart).
* Call the scripts from a crontab, or set it up in monit, etc.

## EXPORT HOME

At line 3 of `kodi-restart.sh`, `kodi-quit.sh`, and `kodi-save-startup.sh` is the 
line `export HOME=/HOME/USER`. Edit this to the appropriate directory to ensure that 
Kodi loads your settings and acts the way you expect it to.

## .kodirc

This file contains only four lines in *this specific order*:

```
The Kodi Host
The port Kodi listens on (the webserver port)
Username
Password
```

## Did I mention to change the filename for the xauth bits?

In both `get_xauth.sh` and `add_xauth.sh`, the filename is listed as `/home/USER/CHANGETHISFILENAME`.  **CHANGE IT**.  Because you're most likely calling `get_xauth.sh` from elsewhere, the path is hardcoded rather than being stored in the same directory (which is the default for everything else).

## Sudoers

If you do not know how to do this, *carefully* follow the instructions here: [https://bobcares.com/blog/how-to-edit-sudoers-file-in-linux-a-perfect-guide-for-you/](https://bobcares.com/blog/how-to-edit-sudoers-file-in-linux-a-perfect-guide-for-you/).  You will want to put these lines at the end, if they do not already exist:  

```
ALL ALL=NOPASSWD:/bin/kill
```

**NOTE: THIS IS A SECURITY RISK. A small one, perhaps, but definitely a risk.**

## Monit example

If you're using [Monit](https://mmonit.com/monit/), a sample configuration file 
would look something like this (changing paths as appropriate, obvs.)

```
check process kodi
        matching "/usr/lib/x86_64-linux-gnu/kodi/kodi.bin"
        start program = "/home/USER/APPS/kodistuff/kodi-safe-startup.sh"
        stop program = "/home/USER/APPS/kodistuff/kodi-quit.sh /home/USER/APPS/kodistuff/.kodirc"
        if failed port KODIPORT then start
```

Not only can `monit` keep track of Kodi now (and restart it, if needed)
