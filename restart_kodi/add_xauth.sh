#!/bin/bash

# Keeps this from overwriting xauth entries when unneeded

check=$(env DISPLAY=:0 xterm -iconic -e exit;echo $?)

if [ $check -ne 0 ];then
    if [ -f /home/USER/CHANGETHISFILENAME ];then
        Display0=$(cat /home/USER/CHANGETHISFILENAME | head -1)
        commandstring=$(printf "/usr/bin/xauth add ${Display0}") 
        eval ${commandstring}
    else
        echo "xauth not properly found."
        exit 99
    fi
fi

