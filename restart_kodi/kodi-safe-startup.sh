#!/bin/bash

# Kodi startup script

SCRIPTDIR="$( cd "$(dirname "$0")" ; pwd -P )"
# Make sure we're part of xauth for display :0, but don't double dip
check=$(env DISPLAY=:0 xterm -iconic -e exit;echo $?)
if [ $check -ne 0 ];then
    "${SCRIPTDIR}"/add_xauth.sh
fi

# initial pause
#sleep 30
MarkerVar=""

# All must be true; incrementing could give false positive returns.
# Yes, you can use systemd to mount these and call kodi.service after it is
# done; this is done this way so that it can work with or without systemd

while [ -z "$MarkerVar" ]; do
    if [ -f /media/_Concerts/favicon.ico ];then
        if [ -f /media/_Movies/favicon.ico ];then
            if [ -f /media/_TV/favicon.ico ];then
                if [ -f /media/_Music/favicon.ico ];then
                    if [ -f /media/_Music/favicon.ico ];then
                        MarkerVar="TRUE"
                    fi
                fi
            fi
        fi
    else
        # If you wish to add a command to mount your external drives, do so here.
        #/bin/mount blah blah blah
        echo "External mounts not available; waiting."
        sleep 10
    fi
done

env DISPLAY=:0 /usr/bin/kodi 
