#!/bin/bash

export HOME=/HOME/USER

SCRIPTDIR="$( cd "$(dirname "$0")" ; pwd -P )"

# Make sure we're part of xauth for display :0, but don't double dip
check=$(env DISPLAY=:0 xterm -iconic -e exit;echo $?)
if [ $check -ne 0 ];then
    "${SCRIPTDIR}"/add_xauth.sh
fi

"${SCRIPTDIR}"/kodi-quit.sh "${SCRIPTDIR}"/.kodirc
sleep 30
# checking to make sure it's down
PID=""
PID=$("${SCRIPTDIR}"/psx kodi.bin | awk '{print $2}')
echo "$PID"
if [ -n "$PID" ];then
    sudo kill "$PID"
    sleep 10
    if [ -n "$PID" ];then
        sudo kill -9 "$PID"
    fi
fi

"${SCRIPTDIR}"/kodi-safe-startup.sh
