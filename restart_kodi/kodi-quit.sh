#!/bin/bash

export HOME=/HOME/USER

SCRIPTDIR="$( cd "$(dirname "$0")" ; pwd -P )"

## Configure your KODI RPC details here

if [ -n "$1" ];then
    RCFILE="$1"
    if [ ! -f "$RCFILE" ];then
        RCFILE=""${SCRIPTDIR}"/.kodirc"
    fi
else
    RCFILE=""${SCRIPTDIR}"/.kodirc"
fi

KODI_HOST=
KODI_PORT=
KODI_USER=
KODI_PASS=
LOCK=false

# If the script does not have configuration hardcoded, the script will
# search for it in $HOME/.kodirc (or the file passed as the first argument)

if [ -z "$KODI_HOST" ]; then
    if [ -f "$RCFILE" ];then
        readarray -t line < "$RCFILE"
        KODI_HOST=${line[0]}
        KODI_PORT=${line[1]}
        KODI_USER=${line[2]}
        KODI_PASS=${line[3]}
    fi
fi

function show_help {
echo "kodi-quit.sh [-h | /path/to/kodirc]" 
echo "This is a massively specialized version of kodi-cli." 
echo "It is for shutting down Kodi, and is meant to be called by a systemd unit, "
echo "for example, as part of a restart or shutdown script. Defaults to using "
echo "$HOME/.kodrc; if you wish to use a different rc file, specify it with "
echo "the full path as the first argument." 
}

if [ "$1" == "-h" ];then
    show_help
    exit 0
fi


# Ensure some configuration is loaded, or exit.
if [ -z "$KODI_HOST" ]; then
    echo ".kodirc not properly set up; host not parseable."
    echo "  "
    show_help
    exit 99
fi

function xbmc_req {
    output=$(curl -s -i -X POST --header "Content-Type: application/json" -d "$1" http://$KODI_USER:$KODI_PASS@$KODI_HOST:$KODI_PORT/jsonrpc)
    if [[ $2 = true ]];
    then
        echo $output
    fi
}

function parse_json {
    key=$1
    awk -F"[,:}]" '{for(i=1;i<=NF;i++){if($i~/'$key'\042/){print $(i+1)}}}' | tr -d '"'
}

function shutdown_kodi {
    echo "Shutting down Kodi"
    xbmc_req '{"jsonrpc": "2.0", "method": "Application.Quit", "id": "mybash"}'
}

shutdown_kodi
